#pragma once
#include <map>
#include "ArenaInterface.h"
#include "FighterInterface.h"

const int MAX_FIGHTERS = 20;
class Arena : public ArenaInterface
{
private:
	std::map<string, FighterInterface*> fighters;
	int nFighters;
public:
	Arena();
	virtual bool addFighter(string info);
	virtual bool removeFighter(string name);
	virtual FighterInterface* getFighter(string name);
	virtual int getSize();
	~Arena();

};

