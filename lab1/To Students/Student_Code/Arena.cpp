#define _CRT_SECURE_NO_WARNINGS

#include <cstring>
#include <regex>
#include "Arena.h"
#include "Fighter.h"
#include "Archer.h"
#include "Cleric.h"
#include "Robot.h"

using namespace std;

Arena::Arena()
{
	//fighters = ;
	nFighters = 0;
}

bool Arena::addFighter(string info)
{
	// check the format
	regex format ("^[A-Za-z]+ [ACR] \\d+ \\d+ \\d+ \\d+$");
	if (!regex_match(info, format))
	{
		return false;
	}

	char *tokens;
	char *toTokenize = new char;

	strcpy(toTokenize, info.c_str());

	tokens = strtok(toTokenize, " ");
	char *c_name = tokens;
	string name = c_name;

	// check that name is not in collection
	map<string, FighterInterface*>::iterator it = fighters.find(name);
	if (it != fighters.end())
	{
		return false;
	}

	tokens = strtok(NULL, " ");
	char *c_type = tokens;
	string type = c_type;

	if (type == "R")
	{
		fighters[name] = new Robot(info);
	}
	else if (type == "A")
	{
		fighters[name] = new Archer(info);
	}
	else if (type == "C")
	{
		fighters[name] = new Cleric(info);
	}

	nFighters++;
	return true;
}

bool Arena::removeFighter(string name)
{
	map<string, FighterInterface*>::iterator it = fighters.find(name);
	if (it == fighters.end())
	{
		return false;
	}

	fighters.erase(name);
	return true;
}

FighterInterface * Arena::getFighter(string name)
{
	map<string, FighterInterface*>::iterator it = fighters.find(name);
	if (it == fighters.end())
	{
		return NULL;
	}

	return fighters[name];
}

int Arena::getSize()
{
	return nFighters;
}



Arena::~Arena()
{
	fighters.clear();
	//delete fighters;
}
