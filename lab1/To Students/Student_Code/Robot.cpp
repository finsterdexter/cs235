#include <cmath>
#include "Robot.h"


Robot::Robot(string info) : Fighter(info)
{
}

int Robot::getDamage()
{
	int total = strength + bonusDamage;
	bonusDamage = 0;
	return total;
}

void Robot::reset()
{
	Fighter::reset();
	bonusDamage = 0;
	currentEnergy = magic * 2;
}

bool Robot::useAbility()
{
	int bonusAmount = (int) strength * pow((double)currentEnergy / ((double)magic * 2.0), 4.0);
	if (currentEnergy >= ROBOT_ABILITY_COST)
	{
		currentEnergy -= ROBOT_ABILITY_COST;
		bonusDamage = bonusAmount;
		return true;
	}
	else
	{
		return false;
	}
}

Robot::~Robot()
{
}
