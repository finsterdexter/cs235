#include "Cleric.h"


Cleric::Cleric(string info) : Fighter(info)
{
}

int Cleric::getDamage()
{
	return magic;
}

void Cleric::reset()
{
	Fighter::reset();
	currentMana = magic * 5;
}

void Cleric::regenerate()
{
	Fighter::regenerate();
	int regainedMana = magic / 5;
	if (regainedMana < 1)
	{
		regainedMana = 1;
	}

	currentMana += regainedMana;
	if (currentMana > magic * 5)
	{
		currentMana = magic * 5;
	}
}

bool Cleric::useAbility()
{
	int healAmount = magic / 3;
	if (healAmount < 1)
	{
		healAmount = 1;
	}

	if (currentMana >= CLERIC_ABILITY_COST)
	{
		currentHP += healAmount;
		if (currentHP > maximumHP)
		{
			currentHP = maximumHP;
		}
		currentMana -= CLERIC_ABILITY_COST;
		return true;
	}
	else
	{
		return false;
	}
}

Cleric::~Cleric()
{
}
