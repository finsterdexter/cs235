#include "Archer.h"


Archer::Archer(string info) : Fighter(info)
{
	baseSpeed = speed;
}

int Archer::getDamage()
{
	return speed;
}

void Archer::reset()
{
	Fighter::reset();
	speed = baseSpeed;
}

bool Archer::useAbility()
{
	speed += 1;
	return true;
}

Archer::~Archer()
{
}
