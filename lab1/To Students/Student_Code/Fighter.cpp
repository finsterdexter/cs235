#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <cstring>
#include "Fighter.h"

using namespace std;

Fighter::Fighter()
{

}
Fighter::Fighter(string info)
{
	char *tokens;
	char *toTokenize = new char;

	strcpy(toTokenize, info.c_str());

	tokens = strtok(toTokenize, " ");
	name = tokens;

	tokens = strtok(NULL, " ");
	string type = tokens;

	tokens = strtok(NULL, " ");
	maximumHP = atoi(tokens);
	currentHP = maximumHP;

	tokens = strtok(NULL, " ");
	strength = atoi(tokens);

	tokens = strtok(NULL, " ");
	speed = atoi(tokens);

	tokens = strtok(NULL, " ");
	magic = atoi(tokens);
}

string Fighter::getName()
{
	return name;
}

int Fighter::getMaximumHP()
{
	return maximumHP;
}

int Fighter::getCurrentHP()
{
	return currentHP;
}

int Fighter::getStrength()
{
	return strength;
}

int Fighter::getSpeed()
{
	return speed;
}

int Fighter::getMagic()
{
	return magic;
}

void Fighter::reset()
{
	currentHP = maximumHP;
}

void Fighter::takeDamage(int damage)
{
	int adjustedDmg = damage - speed / 4;
	if (adjustedDmg < 1)
	{
		adjustedDmg = 1;
	}
	currentHP = currentHP - adjustedDmg;
}

void Fighter::regenerate()
{
	int healAmount = strength / 6;
	if (healAmount < 1)
	{
		healAmount = 1;
	}

	currentHP += healAmount;
	if (currentHP > maximumHP)
	{
		currentHP = maximumHP;
	}
}

bool Fighter::isSimplified()
{
	return true;
}



Fighter::~Fighter()
{

}