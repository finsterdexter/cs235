#pragma once
#include "FighterInterface.h"

class Fighter : public FighterInterface
{
public:
	string name;
	int maximumHP;
	int currentHP;
	int strength;
	int speed;
	int magic;
	Fighter();
	Fighter(string info);
	virtual ~Fighter();

	/*
	getName()

	Returns the name of this fighter.
	*/
	virtual string getName();

	/*
	getMaximumHP()

	Returns the maximum hit points of this fighter.
	*/
	virtual int getMaximumHP();

	/*
	getCurrentHP()

	Returns the current hit points of this fighter.
	*/
	virtual int getCurrentHP();

	/*
	getStrength()

	Returns the strength stat of this fighter.
	*/
	virtual int getStrength();

	/*
	getSpeed()

	Returns the speed stat of this fighter.
	*/
	virtual int getSpeed();

	/*
	getMagic()

	Returns the magic stat of this fighter.
	*/
	virtual int getMagic();

	/*
	takeDamage(int)

	Reduces the fighter's current hit points by an amount equal to the given
	damage minus one fourth of the fighter's speed.  This method must reduce
	the fighter's current hit points by at least one.  It is acceptable for
	this method to give the fighter negative current hit points.

	Examples:
	damage=10, speed=7		=> damage_taken=9
	damage=10, speed=9		=> damage_taken=8
	damage=10, speed=50		=> damage_taken=1
	*/
	virtual void takeDamage(int damage);

	/*
	reset()

	Restores a fighter's current hit points to its maximum hit points.

	Robot:
	Also restores a Robot's current energy to its maximum value (which is 2 times its magic).
	Also resets a Robot's bonus damage to 0.

	Archer:
	Also resets an Archer's current speed to its original value.

	Cleric:
	Also restores a Cleric's current mana to its maximum value (which is 5 times its magic).
	*/
	virtual void reset();

	/*
	regenerate()

	Increases the fighter's current hit points by an amount equal to one sixth of
	the fighter's strength.  This method must increase the fighter's current hit
	points by at least one.  Do not allow the current hit points to exceed the
	maximum hit points.

	Cleric:
	Also increases a Cleric's current mana by an amount equal to one fifth of the
	Cleric's magic.  This method must increase the Cleric's current mana by at
	least one.  Do not allow the current mana to exceed the maximum mana.
	*/
	virtual void regenerate();

	/*
	isSimplified()

	Returns true if you have not completed the useAbility() and/or the regenerate()
	methods for this type of fighter.  This allows the test driver to award points
	for Part 3 even if Part 4 is incomplete.

	Return true if the testing is to be simplified; false otherwise.
	*/
	virtual bool isSimplified();

};